from sanic import Sanic
from sanic.response import json
import records

# 获取app实例
app = Sanic.get_app()

# 配置信息
config = app.config

# https://github.com/kennethreitz/records
db = config['DB_CONN']