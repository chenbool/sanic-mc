# from sanic import Sanic
# from sanic.response import json
#
# app = Sanic("app")
#
# @app.route('/')
# async def test(request):
#     return json({'hello': 'world'})
#


from app import create_app
from app.config import config

# 初始化
app = create_app(config)

if __name__ == '__main__':
    app.run(
        port=5000,
        debug=False,
        access_log=False,
        auto_reload=True,
        workers=4
    )