# 读取配置
from dotenv import dotenv_values

config = {
    'version': '1.0',
    'baseUrl': '127.0.0.1',

    # 是否支持多模块
    'multi_module': False,

    # 模块列表
    'module': {
        'index': '/',
        'admin': '/admin',
        'api': '/api'
    },
    # 静态资源存放目录
    'static': './static',
    # 是否开启监听器
    # 'listen': False
}

# 读取 .env
env = dotenv_values('.env')
# 合并两个列表
config = dict(env, **config)