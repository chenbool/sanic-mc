from sanic import Blueprint
from sanic.response import json
from app.model import Moji


bp = Blueprint(__name__.replace('.', '_'))

@bp.get("/")
async def index(request):
    # user = await Moji.create(name="New User")
    res = await Moji.all()
    for item in res:
        print(item)
    return json({'hello': 'world'})

@bp.get("/edit/<id:int>")
async def edit(request, id):
    return json({'id': id})